/** * 
 *  Controlador  de Angular 
 *  UTF-8 28/11/2020 
 *  landing
 *  Pablo N. Garcia Solanellas
 *  Corrientes, Argentina.
 **/

controllers.controller('LandingCtrl',
['$scope', 'AssetsFactory', 'ModelFactory',
    'ObjectListFactory', '$routeParams', '$filter', 'AnalyticsFactory',
function ($scope, AssetsFactory, ModelFactory,
    ObjectListFactory, $routeParams, $filter, AnalyticsFactory)
{
//VARIABLES PRIVADAS*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
    var _im = {
        caption: 'Landing',
        name: 'landing',
        requiredAssets: [],
        alreadyInitFlag: false,
        preinitInitFlag:false,
        pub: {}
    };
    var _defaults = {};
    _defaults[_im.name] = {};

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//VARIALBES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-        
    $scope.forms[_im.name] = angular.copy(_defaults[_im.name]);
    $scope.inner = {
        data:{},
        ORDEN_BY: {
            DIRECTION: "-",
            COLUM: "id"
        }
    };
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    $scope.swLandingView = function (p_index, p_data) {
        $scope[_im.name].view = $scope[_im.name].views.list[p_index];
        $scope.main.view = $scope[_im.name].view;
        if (typeof p_data !== "undefined")
        {
            $scope.forms[$scope[_im.name].view.name] = angular.copy(p_data);
            //if ($scope[_im.name].view.updatePath){$location.update_path( $filter('locationIdFilter')($scope[_im.name].view, p_data)); }       
        } else {
            $scope.forms[$scope[_im.name].view.name] = angular.copy(_defaults[$scope[_im.name].view.name]);
        }
        //$scope.broadCastManager($scope[_im.name].view.eventName);
        //iniciar select2
        if ($scope[_im.name].view.hasOwnProperty("smartSelect")) {
            angular.forEach($scope[_im.name].view.smartSelect, function (_object) {
                var _selectId = Object.keys(_object)[0];
                _initSelect2(_selectId, _object[_selectId]);
            });
        }
        /*if (_im.alreadyInitFlag) {
         $location.update_path( $filter('locationIdFilter')($scope[_im.name].view, p_data));
         }*/
    };
    $scope.order = function (p_colum) {
        if ($scope.inner.ORDEN_BY.COLUM === p_colum) {
            $scope.inner.ORDEN_BY.DIRECTION = $scope.inner.ORDEN_BY.DIRECTION === "-" ? "+" : "-";
        } else {
            $scope.inner.ORDEN_BY.DIRECTION = "+";
        }
        $scope.inner.ORDEN_BY.COLUM = p_colum;
    };
    $scope.edit = function (p_id) {
        var _data = $filter("objectFilter")($scope.assets.landing, p_id);
        $scope.swLandingView($scope[_im.name].views.keys.landing, _data);
        $scope.msgbox();
    };
    $scope.saveLanding = function () {
        if ($scope.forms[$scope[_im.name].view.name].$valid) {
            _innerSave();
        }
    };
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES privadas*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    var _innerSave = function () {
        $scope.msgbox('espere', _im.caption, 'Guardando...');
        var _innerOnSuccess = function (p_response)
        {
            if (p_response.saved)
            {
                $scope.msgbox('succes', _im.caption,
                        'La informacion se guardo correctamente.'
                        );
                $scope.forms[$scope[_im.name].view.name] = p_response[$scope[_im.name].view.name];
                ObjectListFactory.update(
                        $scope.forms[$scope[_im.name].view.name],
                        $scope.assets.landing
                        );
            }
        };
        ServiceFactory.post("services/landing/save",
                $scope.forms[$scope[_im.name].view.name]
                ).then(_innerOnSuccess, $scope.errorHandler);
    };
    var _onLoadModel = function (p_model) {
        angular.forEach(p_model.forms, function (_element, _key) {
            _defaults[_key] = p_model.forms[_key];            
            $scope.forms[_key] = angular.copy(_defaults[_key]);
        });
        _im.requiredAssets = p_model.config.requiredAssets;
        $scope.inner.data = p_model.data;
        $scope[_im.name] = p_model.pub;
        AssetsFactory.refresh(_im.requiredAssets, $scope.assets).then(_init);
    };
    var _initSelect2 = function (p_selectId, p_attr) {
        $timeout(function () {
            $("#" + p_selectId).select2().on("select2:select", function (e) {
                $scope.forms[$scope[_im.name].view.name][p_attr] = parseFloat(e.params.data.id.replace("number:", ""));
            });
        }, 1000);
    };
//LAST FUNCTIONS init | preInit-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    var _preInit = function () {
        if ($scope.statics.hasOwnProperty('auth') && !_im.preinitInitFlag) {
            _im.preinitInitFlag = true;
            ModelFactory.get(_im.name, 60, "days").then(_onLoadModel);
        }
    };
    var _init = function () {
        if (AssetsFactory.areReady($scope.assets, _im.requiredAssets)) {            
            AnalyticsFactory.view("landing");
            _im.alreadyInitFlag = true;
            if (!isNaN(parseInt($routeParams.id)))
            {
                $scope.edit(parseInt($routeParams.id));
            } else {
                $scope.swLandingView($scope[_im.name].views.keys.heramientas);
                $scope.msgbox();
            }
        }
    };    
    $scope.msgbox('espere', _im.caption, 'Cargando...');
    _preInit();
//$scope.$on("SOME_EVENT",_init);
    $scope.$on("LOAD_STATIC_ASSETS_COMPLETE", _preInit);


}]);


