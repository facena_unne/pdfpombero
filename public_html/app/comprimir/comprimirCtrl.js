/** * 
 *  Controlador  de Angular 
 *  UTF-8 28/11/2020 
 *  comprimir
 *  Pablo N. Garcia Solanellas
 *  Corrientes, Argentina.
 *  
 *  VER comparcion entre webp y jpg y png: https://ieeexplore.ieee.org/document/8301939
 *  https://developers.google.com/speed/webp#:~:text=WebP%20lossless%20images%20are%2026,at%20equivalent%20SSIM%20quality%20index.&text=For%20cases%20when%20lossy%20RGB,file%20sizes%20compared%20to%20PNG.
 **/

controllers.controller('ComprimirCtrl',
['$scope', 'AssetsFactory', 'ModelFactory',
'AnalyticsFactory', '$routeParams', '$filter', '$timeout','$window',
function ($scope,  AssetsFactory, ModelFactory,
AnalyticsFactory, $routeParams, $filter, $timeout, $window)
{
//VARIABLES PRIVADAS*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
    var _im = {
        caption: 'Comprimir',
        name: 'comprimir',
        requiredAssets: [],
        alreadyInitFlag: false,
        preInitFlag: false,
        pub: {}
    };
    var _defaults = {};
    _defaults[_im.name] = {};

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
    //VARIALBES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-            
    $scope.data[_im.name] = angular.copy(_defaults[_im.name]);
    $scope.inner = {
        showUploadButton:true,        
        PDF_PAGE_RENDERING_IN_PROGRESS:0,        
        ORDEN_BY: {
            DIRECTION: "-",
            COLUM: "id"
        }
    };
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
    //FUNCRIONES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    $scope.swComprimirView = function (p_index, p_data) {
        $scope[_im.name].view = $scope[_im.name].views.list[p_index];
        if (typeof p_data !== "undefined")
        {
            $scope.data[$scope[_im.name].view.name] = angular.copy(p_data);            
        } else {
            $scope.data[$scope[_im.name].view.name] = angular.copy(_defaults[$scope[_im.name].view.name]);
        }
    };
    
    /**
     * Lee el achivo contenido en el objetivo del que proviene el evento.
     * @param {type} p_event 
     * @returns {undefined}
     */
    $scope.readFile = function (p_event) {        
        let _file = p_event.target.files[0];        
        
        //Verificamos si el archivo es de alguno de los tipos soportados       
        if($scope.statics.suportFileTypes.indexOf(_file.type) !== -1) {
            //hacemos un backup de la informacion original del archivo.
            $scope.data[$scope[_im.name].view.name].inputFile={
                name:_file.name,
                size:_file.size   
            } ;
            
            //si el archivo es un PDF
            //mostramos el archivo.
            $scope.showPDF( createObjectURL( _file ) );
            
            //fix viewpor size.                        
            //TODO: bind to windows.size to dinamic resize.
            $scope.statics.viewport.size = $("#ref-col").width();
            
            //lanzamos el evento readFileSucces sobre esciviento value
            AnalyticsFactory.event(
                "readFileSucces",
                {                    
                    file_size: $filter("smartFileSizeInfo")(_file.size) 
                }
            );
            
        }else{
            //No es un PDF informamos al usuario.
            $scope.msgbox('danger','Tipo de archivo', 'El archivo seleccionado no es un PDF.');                        
            AnalyticsFactory.event("readFileFail",{file_extension:_file.type});
        }        
    };
    
    $scope.openBrowseWindow=function(){
        $("#file-to-upload").trigger('click');
        
        AnalyticsFactory.event("openOSFileExplorer", {platform: $window.navigator.platform});
        
        /*let el = document.getElementById('file-to-upload');
        angular.element(el).triggerHandler('click');*/
    };    
    
    $scope.next=function(){
        $scope.showPage(++$scope.data[$scope[_im.name].view.name].paginaActual);
    }; 
    
    $scope.prev=function(){
        $scope.showPage(--$scope.data[$scope[_im.name].view.name].paginaActual);
    }; 
    
    /**
     * Aumenta o disminuye el tamaño del camvas que contien la pagina.
     * @param {integer} p_veces si es positivo incrementamos el zoom la cantidad indicada en [p_veces]
     * el valor indicado por zoomStep en el modelo.
     * Si es negativo decrementa la cantidad indicada en [p_veces] el tamaño del contenedor.
     * @returns {undefined}
     */
    $scope.zoom=function(p_veces){
        $scope.statics.viewport.size+= $scope.data[$scope[_im.name].view.name].zoomStep * p_veces;
        _showALLPage();
    };
    /**
     * Muestra el archivo PDF seleccionado.
     * @param {Object (URL)} pdf_url
     * @throws {exceptionType} En caso de que haya algun error al generar el documento.
     * @returns {undefined}
     */
    $scope.showPDF=function(pdf_url) {
        
        var _onSuccess=function(pdf_doc) {
            //$scope.data[$scope[_im.name].view.name].showUpload = false;
            $scope.$apply(function(){
                $scope.data[$scope[_im.name].view.name].doc = pdf_doc;    
                //hide upluad options
                $scope.data[$scope[_im.name].view.name].showUpload=false;  
                
                $scope.data[$scope[_im.name].view.name].cnvPages=[];
                for (var i = 0; i < $scope.data[$scope[_im.name].view.name].doc.numPages; i++) {
                    $scope.data[$scope[_im.name].view.name].cnvPages.push({index:i,show:true});
                }        
                
            });                        
            _showALLPage();       
            //seteamos el numero de paginas del documento
            
            //disparamos el evento
            AnalyticsFactory.event(
                "onShowFileSuccess",
                {number_of_page: $scope.data[$scope[_im.name].view.name].doc.numPages}
            );
        };
        
        
        var _onError=function(error){            
            //mostramos el mensage
            $scope.errorHandler(error);
            //volvemos a la vista de subir archivo.
            $scope.swComprimirView($scope[_im.name].views.keys.upload);
            //seteamos la descripcion del error
            
            AnalyticsFactory.event("onShowFileFail",{description: error.description});
        };
        
        
        //generamos el documento.
        PDFJS.getDocument({ url: pdf_url }).then(_onSuccess).catch(_onError);
        
    };
    
    
    /**
     * @param {double} p_quality calida esperada del documento varia entre 0 y 1
     * siendo 1 la maxima calidad y 0 la minima. 
     * @returns {undefined}
     */
    $scope.compressDocument=function( p_quality  )
    {   
        
        if ($filter("thereIsSomePage")($scope.data.upload.cnvPages)){
            $scope.msgbox('espere', _im.caption, 'Iniciando compresión.');
            _compressPages(p_quality);             
        }else{
            
            let _msg = "Debe incluir al menos una página.";
            let _type = 'warning';
            $scope.msgbox(_type, _im.caption, 
                _msg
            );
            AnalyticsFactory.event(
                "notification_foreground",    
                {
                    "message_name":"empty file", 
                    "topic":"compressed file", 
                    "label": _im.caption, 
                    "message_type":_type
                }
            );
        }
        
    };
    
    /**
     * Comprime la pagina del pdf de entrada pasada como parametro
     * @param {integer} p_pageIndex 0, ..., n
     * @returns {undefined}
     */
    $scope.compressImageAndDownload=function(p_pageIndex){        
        
        //Envoltorio para enviar el numero de pagina a la funcion de codificacion.
        let _wrapper=function(p_blob){            
            //agregamos informacion sobre las dimenciones de la imagen.
            //util para ajustes de tamaño.
            p_blob.width= _canvas.width;
            p_blob.height= _canvas.height;            
            _compressPage(p_blob,p_pageIndex);
        };
        
        //identificador del canvas que contien la pagia a descargar.
        let _canvasId = 'pdf-canvas-page-'+ p_pageIndex;
        var _canvas = document.getElementById(_canvasId);        
        _canvas.toBlob(_wrapper,"image/jpeg", 1);
	
    };
    /**
     * Inicia la descarga del archivo de salidad comprimido.
     * El nombre por defecto del nuevo archivo incluye una marca de tiempo para
     * fasilitar al usuario su identificacion.
     * @returns {undefined}
     */
    $scope.downloadPDF=function(){
        let _timestamp = moment().format("MMDDHHmmss");
        $scope.data.outputPDF.save(_timestamp +"_"+$scope.data.upload.inputFile.name+ "_COMP.pdf"); 
        
        
        AnalyticsFactory.event(
            "downloadNewPDFFile",
            {
                output_file_num_page:$filter("outputFileNumPage")($scope.edata.upload.cnvPages),
                file_size: $filter("smartFileSizeInfo")($scope.data[$scope[_im.name].view.name].file.size)
            }
        );
    };
    $scope.setPageVisivility=function(p_page){
        p_page.show = !p_page.show;
        let _analyticData ={
            input_file_num_page:$scope.data[$scope[_im.name].view.name].doc.numPages,
            output_file_num_page:$filter("outputFileNumPage")($scope.edata.upload.cnvPages)
        };
        if( p_page.show){
            //visicible
            AnalyticsFactory.event("keepOutPageFromOutputPDF",_analyticData);
        }else{
            //oculta
            AnalyticsFactory.event("restorePageFromOutputPDF",_analyticData);
        }
        
    };
    $scope.backToUploadView=function(){
        $scope.swComprimirView($scope.comprimir.views.keys.upload);        
    };
    
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
    //FUNCRIONES privadas*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*   
    /**
     * Comprime una pagina seleccionada.
     * @param {Objeto Binario Grande} blobPage contien la pagina
     * @param {integuer} p_pageIndex pocicion de la pagina en el arreglo de paginas. 0 ... n
     * @returns {undefined}
     */
    var _compressPage=function(blobPage,p_pageIndex){
        
        //minima compresion de la imagen.
        //puede ajustar el valor de sizeRatio para reducir el tamaño de la imagen.
        let _image={format:"jpeg", quality:0.9, sizeRatio:1};        
        let _onSuccess=function(result) {            
            let downloadLink = document.createElement("a");
            let _timestamp = moment().format("YYMMDDHHmmss");
            //sumamos uno al indice de pagina para combertir el indice en la posicion ordinal de la pagina.
            downloadLink.download = _timestamp + "_pagina_"+(p_pageIndex+1)+ "."+_image.format;
            downloadLink.href =createObjectURL(result);
            downloadLink.click();    
            
            AnalyticsFactory.event("downloadSinglePage",
                {
                    file_extension: _image.format,
                    file_size: result.size,
                    file_quality: _image.quality
                }
            );
        };        
        let compressor = new Compressor(blobPage, {
            quality: _image.quality,
            maxWidth: blobPage.width*_image.sizeRatio,            
            mimeType:"image/"+_image.format,
            success:_onSuccess,
            error:$scope.errorHandler
        });
    };
    /**
     * Comprime todas las paginas activas. 
     * se considera una página activa a la página que el usuario desea se incluya
     * en el documento final. por defecto todas las páginas están incluidas.
     * @param {double} p_quality  varia entre 0.0 y 1.0, reprecenta la calidad 
     * de la imagen luego de la compresión  en relación a la imagen origina. 
     * @returns {undefined}
     */
    var _compressPages=function(p_quality){
        let _images={list:[], size:0};        
        let _iteraciones=1;
        
        let _onSuccess=function(result, p_pageIndex) {          
            let _obj= {
                blob:result,
                size:result.size,
                order:p_pageIndex
            };
            //agregamos la imagen en formato de URL en el objeto            
            _images.list.push(_obj);            
            if (_iteraciones>=$scope.data[$scope[_im.name].view.name].doc.numPages){        
                $scope.msgbox('espere', _im.caption, 'Creando PDF.');                
                $timeout(function(){
                    _compilePDFfile( _images );
                },0);
            }else{
                $scope.msgbox('espere', _im.caption, 
                    'Comprimiento páginas: ' + ++_iteraciones +" de " +$scope.data[$scope[_im.name].view.name].doc.numPages
                );
            }
        };
        
        let _innerCompress=function(p_objectBlob,p_pageIndex){
            let compressor = new Compressor(p_objectBlob, {
                quality: p_quality,
                mimeType:"image/jpeg",
                success:function(result){
                    _onSuccess(result,p_pageIndex);
                }
            });
        };
        
        
        for (var _pageIndex = 0; _pageIndex < $scope.data[$scope[_im.name].view.name].doc.numPages; _pageIndex++) {         
            
            let _canvasId = 'pdf-canvas-page-'+ _pageIndex;            
            var _canvas = document.getElementById(_canvasId);        
            _canvas.toBlob(function(p_objectBlob){
                _innerCompress(p_objectBlob, _pageIndex);
            },"image/jpeg", 1);                                        
        };        
        
        
        AnalyticsFactory.event("selectCompressionRate",
            {
                file_quality: p_quality
            }
        );
        
    };
    var _compilePDFfile=function(p_element)
    {
        //creamos un nuevo archivo pdf para contener el resultado de la compresión.
        $scope.data.outputPDF = new jspdf.jsPDF( $scope[_im.name].pfdOptios );     
        
        //Nos haceguramos que las paginas estan ordenadas.
        $filter('orderBy')(p_element.list,'+order');
        
        let _outputFileNumPages=0;
        //iteramos el arreglo de paginas para poblar el PDF.
        angular.forEach(p_element.list,function(_image,_index){
            
            //excluimos las paginas ocultas del archivo final.
            if (!$scope.data.upload.cnvPages[_index].show) return;
            
            //contamos el numero de paginas
            _outputFileNumPages++;
            //acumulamos el peso de las imagenes para estimar el tamaño final del archivo.
            p_element.size+=_image.size;            
            //agregamos la imagen al nuevo PDF
            $scope.data.outputPDF.addImage(createObjectURL(_image.blob), "JPEG",-5,-5);                                                            
            
            //Agregamos hojas, si no es el ultimo elemento
            if(_index<(p_element.list.length)-1){
                //Agregamos un hoja mas
                $scope.data.outputPDF.addPage("a4", "p");
            };            
        });
        var _data = angular.copy(_defaults.download);
        //sumamos a las imagenes el peso estimado de la metadata del arhivo PDF creado
        //TODO: mejorar esta estimacion, si es posible.
        let _newFileSize = (p_element.size+4300);
        
        _data.file={   
            numPages:_outputFileNumPages,
            size : _newFileSize,
            inputFileSize:$filter("smartFileSizeInfo")($scope.data.upload.inputFile.size),
            compressionRate:$filter("tasaDeCompresion")(_newFileSize, $scope.data.upload.inputFile.size)
        };
        
        
        let _size= parseFloat(_data.file.compressionRate).toFixed(2);
        if (_size>0){
            $scope.swComprimirView($scope[_im.name].views.keys.download,_data);
            $scope.msgbox();            
            
            AnalyticsFactory.event("compresionResult",
                {
                "input_file_num_page": $scope.data.upload.doc.numPages,
                "output_file_num_page": _data.file.numPages,                
                "file_compression_rate":_data.file.compressionRate,
                "file_size":_data.file.size
                }
            );
        }else{
            $scope.msgbox('danger', _im.caption, 
                "Lo sentimos, pero no hemos podido optimizar el peso de este archivo."
                +"\n Puede que este archivo ya este optimizado o contenga información que no se puede comprimir."
            );           
            
            //Setteamos el resultado de la compresión.            
            AnalyticsFactory.event("compresionFail", {value: parseInt( _size ) });
             AnalyticsFactory.event("compresionFail",
                {
                "input_file_num_page": $scope.data.upload.doc.numPages,                
                "file_growth_rate":_data.file.compressionRate,
                "file_size":_data.file.size
                }
            );
        }        
    };
    var _showALLPage=function() {        
        $scope.inner.PDF_PAGE_RENDERING_IN_PROGRESS = 1; //flag 
        for (var i = 1; i <= $scope.data[$scope[_im.name].view.name].doc.numPages; i++) {         
            $scope.data[$scope[_im.name].view.name].doc.getPage(i).then(function(p_page){
                //$scope.data[$scope[_im.name].view.name].canvasArray.
                let _name = '#pdf-canvas-page-'+ p_page.pageIndex;
                let _innerPdfCanvas  = $(_name).get(0);
                let _innerPdfCanvasCtx = _innerPdfCanvas.getContext('2d');

                // As the canvas is of a fixed width we need to set the scale of the viewport accordingly
                var scale_required = _innerPdfCanvas.width / p_page.getViewport(1).width;

                // Get viewport of the page at required scale
                var viewport = p_page.getViewport(scale_required);

                // Set canvas height
                _innerPdfCanvas.height = viewport.height;        

                var renderContext = {
                    canvasContext: _innerPdfCanvasCtx,
                    viewport: viewport
                };

                // Render the page contents in the canvas
                p_page.render(renderContext).then(function() {
                    $scope.inner.PDF_PAGE_RENDERING_IN_PROGRESS = 0;
                });
            }); 
        };
    };    
    
    var _onLoadModel = function (p_model) {
        angular.forEach(p_model.data, function (_element, _key) {
            _defaults[_key] = p_model.data[_key];            
            $scope.data[_key] = angular.copy(_defaults[_key]);
        });
        
        _im.requiredAssets = p_model.config.requiredAssets;
        $scope[_im.name] = p_model.pub;        
        AssetsFactory.refresh(_im.requiredAssets, $scope.assets).then(_init);
    };
    
    //LAST FUNCTIONS init | preInit-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  
    var _preInit = function () {
        if ($scope.statics.hasOwnProperty('auth') && !_im.preinitInitFlag) {
            _im.preinitInitFlag = true;
            ModelFactory.get(_im.name, 60, "days").then(_onLoadModel);
        }
    };
    var _init = function () {
        if (AssetsFactory.areReady($scope.assets, _im.requiredAssets)) {
            _im.alreadyInitFlag = true;
            AnalyticsFactory.initView(_im.name, $scope.analytics[_im.name]);
            if (!isNaN(parseInt($routeParams.id)))
            {
                $scope.edit(parseInt($routeParams.id));
            } else {
                // Initialization
                $scope.swComprimirView($scope[_im.name].views.keys.upload);                
                $scope.msgbox();
            }
        }
    };
    $scope.msgbox('espere', _im.caption, 'Cargando...');
    _preInit();
    $scope.$on("LOAD_STATIC_ASSETS_COMPLETE", _preInit);
    

}]);


