
/**
 * PDF pombero.
 * @type type
 */

controllers.controller('MainCtrl', 
['$scope','$timeout', '$window',
    'NotifFactory', 'ErrorHandlerFactory','AnalyticsFactory',
    'ServiceFactory',
function($scope, $timeout,$window,
    NotifFactory, ErrorHandlerFactory, AnalyticsFactory, 
    ServiceFactory ) 
{   
    
    $scope.styleFix={"min-height":($window.innerHeight-300)+"px"};
    $scope.messageHeight={"min-height":($window.innerHeight-200)+"px"};
    
    $scope.main={
        title:"PDF Pombero", 
        srcLogo: AppGlobals.SERVER_PATH+"img/logo_x64.png",
        force:false,loading:true,
        accionEnProgreso:"Iniciando.", loadingGif:AppGlobals.SERVER_PATH+"img/loading.gif?c=6",
        innerView:{name:null}
    };    
    $scope.forms={};     
    $scope.data = {};
    
    $scope.status={online:true};
    $scope.assets={};            
    $scope.statics={};//static Assetes    
    $scope.analytics ={};
    $scope.confirmarAccion = NotifFactory.action();    
    $scope.messageStatus = NotifFactory.msgbox();

    $scope.broadCastManager=function(p_eventName,p_arguments){
        $timeout(function(){
            $scope.$broadcast(p_eventName, p_arguments);        
        },0);
    };

    $scope.errorHandler=function(responce){
        var _msg = ErrorHandlerFactory.handler(responce);
        $scope.msgbox(_msg.clss, _msg.title, _msg.text);                     
    };

    $scope.msgbox=function(p_accion, p_titulo, p_mensaje){ 
        $scope.confirmarAccion = NotifFactory.action();      
        $scope.messageStatus=NotifFactory.msgbox(p_accion, p_titulo, p_mensaje);        
        if (!$scope.confirmarAccion.show){ $scope.broadCastManager("POPUP_CLOSE");};
    };    

    $scope.actionPopup=function(p_titulo, p_mensajeArray, p_onOk, p_onCancel ){        
        $scope.messageStatus = NotifFactory.msgbox();      
        $scope.confirmarAccion = NotifFactory.action(p_titulo, p_mensajeArray, p_onOk, p_onCancel);        
        if (!$scope.confirmarAccion.show){ $scope.broadCastManager("POPUP_CLOSE");};
    };
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES privadas*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    var _loadStaticAssets=function(p_response){
        $scope.statics=p_response;
        //cargamos los objetos de seguimiento.
        var _readAnalyticObject=function(p_gaResponse){
            //enlacamos al nexo.
            $scope.analytics = p_gaResponse;
            $scope.broadCastManager("LOAD_STATIC_ASSETS_COMPLETE");
        };
        ServiceFactory.get("assets/json/google-analytics.json").then( _readAnalyticObject);        
    };
    
//LAST FUNCTIONS init | preInit-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    
    var _init=function(){        
        angular.element(document).find("body").removeClass("hidden");
        PRE_INIT_ADMIN_PANEL();
        $scope.main.loading=false;        
    };
    //iniciamos GA4, padamos el G-ID y el path del proyecto.
    AnalyticsFactory.init('G-J9EK78FMED','/'+AppGlobals.PROJECT_NAME);
    //onready
    $scope.$on("LOAD_STATIC_ASSETS_COMPLETE",_init);
    ServiceFactory.get("assets/json/static-assets.json").then( _loadStaticAssets );
    
    
}]);