var createObjectURL = (window.URL || window.webkitURL || {}).createObjectURL || function(){};
Object.exists = function(obj) {
    return typeof obj !== "undefined" && obj !== null;
};
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};


var ngApp = angular.module('ngApp',[
    'controllers','ngRoute',
    'LocalStorageModule',
    'filters'
]);  

moment.locale("es");

var controllers = angular.module('controllers', []);
var filters = angular.module('filters', []);

ngApp.config(
    ['$routeProvider', 'localStorageServiceProvider',
    function( $routeProvider, localStorageServiceProvider ) {    
    
    localStorageServiceProvider.setPrefix(AppGlobals.PROJECT_NAME)
    .setStorageType('localStorage')
    .setNotify(true, true);
   
    
    $routeProvider   
        .when('/landing', {
            templateUrl: 'app/landing/mainView.html?c=TIME_STAMP_JS_FILE_HERE',
            controller: 'LandingCtrl'
        })      
        .when('/comprimir', {
            templateUrl: 'app/comprimir/mainView.html?c=TIME_STAMP_JS_FILE_HERE',
            controller: 'ComprimirCtrl'
        })  
        .when('/about', {
            templateUrl: 'app/about/mainView.html?c=TIME_STAMP_JS_FILE_HERE',
            controller: 'AboutCtrl'
        }) 
        .otherwise({
            redirectTo: '/landing'
        });   
}]);
