/* 
 * Module Design Pattern
 */
var AppGlobals=(function(){
    var pub={};
    pub.PROJECT_NAME="pdfpombero";    
    pub.PROJECT_FOLDER_NAME= location.hostname.indexOf("localhost")!==-1 ?pub.PROJECT_NAME:"";
    pub.COMMON_APP_URL = location.protocol+"//"+location.hostname;    
    pub.SERVER_PATH= pub.COMMON_APP_URL+":"+location.port+"/"+pub.PROJECT_FOLDER_NAME+"/"; 
    pub.APP_VERSION = "TIME_STAMP_JS_FILE_HERE";
    return pub;
})();

