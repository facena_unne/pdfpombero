/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


ngApp.factory('ModelFactory', ['ServiceFactory', '$q', 'localStorageService',
    function (ServiceFactory, $q, localStorageService) {
    
    var pub={};
    var TIME_STAMP_TEXT = "TIME_STAMP"+"_JS_FILE_HERE";
    var _deployment = AppGlobals.APP_VERSION;
    var IS_DEV_ENVIOMENT= location.hostname.indexOf('localhost')!==-1;
    var _getFromServer=function(p_modelName){
        return ServiceFactory.get("app/"+p_modelName+"/models.json?v="+_deployment).then(
            function(p_response){
                p_response.latest = _deployment ;
                pub.save( p_modelName, p_response );
                return p_response;
            },
            function(p_response){
                 return $q.reject(p_response.data);
            }
        );
    };
    pub.get= function( p_modelName, p_deployment ) {
        var _model =localStorageService.get( "model."+p_modelName);        
        if (_model !== null && !IS_DEV_ENVIOMENT){                                
            if ( _deployment === _model.latest && 
                _model.latest !== TIME_STAMP_TEXT ){
                return $q.resolve(_model);  
            }else{
                return _getFromServer(p_modelName);
            }
        }else{
            return _getFromServer(p_modelName);
        }                
    };
    pub.save=function(p_modelName, p_data){        
        return localStorageService.set( "model." + p_modelName, p_data);
    };
        
    return pub;
}]);
