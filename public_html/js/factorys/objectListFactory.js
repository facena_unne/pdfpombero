/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


ngApp.factory('ObjectListFactory', ['$timeout',
    function ($timeout) {    
    var pub={};
    /**
     * Actualiza la lista, en caso de que la clave ya exista actualiza la informacion
     * en el caso de que el objeto no este, lo agrega, llamando a add
     * @param {Object} p_elementToAdd 
     * @param {object} p_object {list:[], keys:{}}     
     * @see {function} pub add
     */
    pub.update=function( p_elementToAdd, p_object ){
        if (p_object.keys.hasOwnProperty(p_elementToAdd.id)){
            if (p_elementToAdd.id === 0){
                pub.add( p_elementToAdd, p_object );
            }else{
                p_object.list[p_object.keys[p_elementToAdd.id]] = p_elementToAdd;    
            }
        }
        else
        {
            pub.add( p_elementToAdd, p_object );
        }        
    };
    
    /**
     * Agrega un objeto al master object y actualiza las keys  
     * @param {Object} p_elementToAdd objecto para agregar al master object
     * @param {Object} p_object master object = { list:[], keys:[] }     
     * @param {boolean} p_top  insertar en el tope o en el final.
     */
    pub.add=function( p_elementToAdd, p_object, p_top ){
        if (p_top){
            p_object.list.unshift(p_elementToAdd);
        }else{
            p_object.list.push(p_elementToAdd);
        }
        p_object.keys={};
        pub.resetKeys(p_object);        
    };
    
    /**
     * 
     * @param {type} p_element item incluido en el objecto
     * @param {type} p_object objeto {list:[],keys:{}}
     * @returns {Object} p_object
     * @see pub.resetKeys()
     */
    pub.remove=function( p_element, p_object ){
        p_object.list.splice(p_object.keys[p_element.id],1);
        pub.resetKeys(p_object);
    };
    
    pub.removeIndex=function( p_index, p_object ){
        p_object.list.splice(p_index,1);
        pub.resetKeys(p_object);
    };
    pub.removerSiExiste=function(p_list, p_object, p_idAttr){                
        var _idxToremove=-1;
        angular.forEach(p_list,function( _element, _index ){
            if (p_object[p_idAttr] === _element[p_idAttr]){
                _idxToremove=_index;
            }            
        });
        if(_idxToremove!==-1){
            p_list.splice(_idxToremove,1);    
            return true;
        }else{
            return false;
        }       
    };
    pub.resetKeys=function(p_object){
        p_object.keys={};
        angular.forEach(p_object.list,function( _element, p_index ){
            p_object.keys[_element.id] = p_index;
        });
    };
    
    
    
    
    return pub;
}]);

