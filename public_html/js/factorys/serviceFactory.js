/** *
 *  Services Factory
 *  UTF-8 28/11/2020 
 *  Contiene funciones POST y GET para invocar servicios
 *  Pablo N. Garcia Solanellas
 *  Corrientes, Argentina.
 */

ngApp.factory('ServiceFactory', ['$http', '$q','$timeout',
    function ($http, $q, $timeout) {
    
    var pub={};
    
    var contextTipeList=[ 
        {'Content-Type':'application/json;charset=utf-8'},
        {'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'},
        {'Content-Type':'application/text;charset=UTF-8'}
    ];
    /**
     * Verifica si el contenido de la respuesta tiene el formato esperado y
     * además verifica si no contiene un error que provenga del servido
     * @param {type} response respuesta de la promesa invocada
     * @returns { response.data || reject }
     */
    var _onSuccess=function(response) {               
        if (typeof response.data === 'object') {                                        
            if (!response.data.hasOwnProperty("error")){
                return response.data;
            }                                            
        }
        //en otro caso falla
        return $q.reject(response.data);
    };
    
    /**
     * Rechaza la respuesta por considerarse errónea
     * @param {type} response
     * @returns {reject}
     */
    var _onError=function(response) {
        return $q.reject(response.data);
    };
   
    pub.get= function( p_service, p_data) { 
        //reques Options
        let _reqOptions = {
            method: "GET",
            url:  p_service.indexOf('://') !== -1 ? p_service : AppGlobals.SERVER_PATH + p_service ,
            headers: contextTipeList[0], 
            params: p_data         
        };
        
        // The $http API is based on the deferred/promise APIs exposed by the
        // $q service so it returns a promise for us by default        
        return $http(_reqOptions).then(_onSuccess, _onError);
    };
    
    pub.post= function(p_service, p_data) {                           
        //reques Options
        let _reqOptions = {
            method:'POST',
            url:  p_service.indexOf('://') !== -1 ? p_service : AppGlobals.SERVER_PATH + p_service,
            data: $.param(p_data),
            headers: contextTipeList[1]
        };
        // the $http API is based on the deferred/promise APIs exposed by the
        //  $q service so it returns a promise for us by default        
        return $http(_reqOptions).then(_onSuccess, _onError);
    };    
        
    return pub;
}]);
