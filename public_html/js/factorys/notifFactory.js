/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


ngApp.factory('NotifFactory', [function () {
    
    var pub={};
    pub.error={};//error handler
    
    pub.msgbox=function(p_accion, p_titulo, p_mensaje){
        var _config ={show:true, waiting:false, message:p_mensaje,title:p_titulo,cssClass:'modal-success'};
        if (typeof p_accion !== "undefined"){
            if (p_accion ==="espere"){            
                _config.waiting = true;            
            }else if(p_accion ==="success" || p_accion ==="ok"){
                _config.cssClass='modal-success';    
            }else if(p_accion ==="danger" || p_accion ==="error"){
                _config.cssClass='modal-danger';
                if (typeof p_mensaje ==="undefined"){_config.message = 'Algo salio mal.';}
            }else if(p_accion ==="warning" || p_accion ==="advertencia"){
                _config.cssClass='modal-warning';
            }else if(p_accion ==="info" ){
                _config.cssClass='modal-info';
            }else if(p_accion.indexOf("modal") !==-1){
                _config.cssClass=p_accion;           
            }
         }else{
            _config.show = false;            
            _config.waiting = false;                    
            setTimeout(function(){
                $(".select2").select2({ width: 'resolve' });
            },1000);
        }
        return _config;
    };
    
    pub.action=function(p_titulo, p_mensajeArray, p_onOk, p_onCancel){
        var _innerOk = function(){};
        var _innerCancel = function(){};
        var _ok={acion:_innerOk,texto:"Si"};
        var _cancel={acion:_innerCancel,texto:"No"};
        var _confirmarAccion={show:false, titulo:'Esta seguro?', mensaje:[], 
            onOk:_ok, onCancel:_cancel
        };
        if(typeof p_titulo !== "undefined" ){
            if (typeof p_mensajeArray !== "object"){
                var _array = new Array();
                _array.push(p_mensajeArray);
                p_mensajeArray = _array;
            };
            if (typeof p_onOk === "object"){_ok = p_onOk;}
            if (typeof p_onCancel === "object"){_cancel = p_onCancel;}
            _confirmarAccion={show:true, titulo:p_titulo, mensaje:p_mensajeArray, onOk:_ok, onCancel:_cancel };
        };      
        
        setTimeout(function(){
            $(".select2").select2({ width: 'resolve' });
        },1200);
        
        return _confirmarAccion;
        
    };
    
    pub.nextCtrlSobre=function(p_modal, p_titulo, p_mensajeArray, p_onOk, p_onCancel, p_ids){
        
        var _innerOk = function(){};
        var _innerCancel = function(){};
        
        var _ok={acion:_innerOk, texto:"Si", css:'btn-default'};
        var _cancel={acion:_innerCancel, texto:"No", css:'btn-primary'};
        if (typeof p_modal === "undefined"){ p_modal =  'modal-primary'; }
        var _objData={
            ids:p_ids,
            css:p_modal,
            show:false, titulo:'Esta seguro?', mensaje:[], 
            onOk:_ok, onCancel:_cancel
        };
        if(typeof p_titulo !== "undefined" ){
            if (typeof p_mensajeArray !== "object"){
                var _array = new Array();
                _array.push(p_mensajeArray);
                p_mensajeArray = _array;
            };
            if (typeof p_onOk === "object"){_ok = p_onOk;}
            if (typeof p_onCancel === "object"){_cancel = p_onCancel;}
            _objData={css:p_modal,show:true, titulo:p_titulo, mensaje:p_mensajeArray, onOk:_ok, onCancel:_cancel};
        };      
        
        setTimeout(function(){
            $(".select2").select2({ width: 'resolve' });
        },1200);
        
        return _objData;
        
    };
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    
    
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-ERRORES*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    
    pub.error.mensaje=function(data){
        var _mensaje = "Algo salio mal.";        
        if (data.hasOwnProperty("error")){
            if (data.error > 1){
                _mensaje = data.description;
            }
        }
        return _mensaje;
        
    };
    
    return pub;
}]);