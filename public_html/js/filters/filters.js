filters.filter('shortenString',
[ function(){    
    var pub =function( p_text,p_maxLength, p_sufijo) {
        if (typeof p_text !=="undefined"){
            if (p_text.length>p_maxLength){
                var _inner = p_text.substring(0,p_maxLength);
                return _inner+ p_sufijo;
            }else{
                return p_text;
            }
        }else{
            return "";
        }        
    };
    return pub;
}]);

filters.filter('smartFileSizeInfo',
[ function(){    
    var pub =function( p_bytes,p_decimals) {
        if (p_bytes === 0) return '0 Bytes';        
        const k = 1024;
        const dm = typeof p_decimals === "undefined" ? 0 : p_decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(p_bytes) / Math.log(k));

        return parseFloat((p_bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];      
    };
    return pub;
}]);

filters.filter('dataURItoBlob', function() {
    var pub=function(dataURI){
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        //Old Code
        //write the ArrayBuffer to a blob, and you're done
        //var bb = new BlobBuilder();
        //bb.append(ab);
        //return bb.getBlob(mimeString);

        //New Code
        return new Blob([ab], {type: mimeString});
    };
    return pub;
});

/**
 * Calcula la tasa de compresión del archivo en funcion de el tamaño del
 * archivo de entrada y el tamaño del archivo comprimido.
 */
filters.filter('tasaDeCompresion',
['$filter', function($filter){
    var pub =function( p_newFileSize, p_oldFileSize ) {
        if(p_newFileSize>= p_oldFileSize)return -1;
        let _porcentaje = (1- (p_newFileSize / p_oldFileSize)) *100;
        return $filter("number")(_porcentaje,2) ;
    };
    return pub;
}]);

/**
 * Is less or equal than max file size of other Apps
 */
filters.filter('leMaxFileSize',
[ function(){    
    var pub =function( p_otherApps, p_newFileSize ) {
       let _array = [];
       angular.forEach(p_otherApps,function(_app){
           if (p_newFileSize<=_app.maxFileSize){_array.push(_app);}
       });
       return _array;
    };
    return pub;
}]);
/**
 * verifica si el arreglo de paginas contien al menos una pagina
 * 
 */

filters.filter('thereIsSomePage',
[ function(){    
    var pub =function( p_array ) {
       let _thereIs = false;
       angular.forEach(p_array,function(_page){
           if (_page.show){_thereIs=true;}
       });
       return _thereIs;
    };
    return pub;
}]);


filters.filter('outputFileNumPage',
[ function(){    
    var pub =function( p_array ) {
       let _thereIs = 0;
       angular.forEach(p_array,function(_page){
           if (_page.show){_thereIs++;}
       });
       return _thereIs;
    };
    return pub;
}]);